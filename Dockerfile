FROM ubuntu:latest

# Add files
ADD run.sh /run.sh
ADD entrypoint_cron.sh /entrypoint_cron.sh

# Install cron
RUN apt-get update && \
	apt-get install -y git cron rsync && \
	chmod +x /run.sh /entrypoint_cron.sh

ENTRYPOINT /entrypoint_cron.sh
