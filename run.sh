#!/bin/bash
timestamp=`date +%Y/%m/%d-%H:%M:%S`
APP_PATH="/app_source/$GIT_SITE_REPO"

if [ -d $APP_PATH ] 
then
    cd $APP_PATH && \
    git pull && \
    echo "pull $timestamp" && \
    sh -c "rsync -a /app_source/${GIT_SITE_REPO}/content/ /app_dest"
else
    cd "/app_source" && \
    git clone https://${GIT_USER}:${GIT_KEY}@${GIT_REPO_URL} --branch ${GIT_BRANCH} --single-branch && \
    echo "INITIALIZED AT $timestamp" && \
    sh -c "rsync -a /app_source/${GIT_SITE_REPO}/content/ /app_dest"
fi
